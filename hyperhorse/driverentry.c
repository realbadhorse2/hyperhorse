//#include <ntddk.h>
#include <ntifs.h>
#include <wdm.h>

#include <intrin.h>


#include "includes.h"

#include "ia32_compact.h"

#include "msr.h"
#include "cpuid.h"
#include "vmcb.h"
#include "cr.h"



DRIVER_UNLOAD DriverUnload;

void DriverUnload(
	PDRIVER_OBJECT DriverObject
)
{
	UNREFERENCED_PARAMETER(DriverObject);
	return;
}

EXTERN_C
VOID
_sgdt(
	_Out_ PVOID Descriptor
);

#include <pshpack1.h>
typedef struct _DESCRIPTOR_TABLE_REGISTER
{
	UINT16 Limit;
	ULONG_PTR Base;
} DESCRIPTOR_TABLE_REGISTER, * PDESCRIPTOR_TABLE_REGISTER;
static_assert(sizeof(DESCRIPTOR_TABLE_REGISTER) == 10,
	"DESCRIPTOR_TABLE_REGISTER Size Mismatch");
#include <poppack.h>

#define RPL_MASK        3
#pragma warning(disable:4201)
typedef struct _SEGMENT_DESCRIPTOR
{
	union
	{
		UINT64 AsUInt64;
		struct
		{
			UINT16 LimitLow;        // [0:15]
			UINT16 BaseLow;         // [16:31]
			UINT32 BaseMiddle : 8;  // [32:39]
			UINT32 Type : 4;        // [40:43]
			UINT32 System : 1;      // [44]
			UINT32 Dpl : 2;         // [45:46]
			UINT32 Present : 1;     // [47]
			UINT32 LimitHigh : 4;   // [48:51]
			UINT32 Avl : 1;         // [52]
			UINT32 LongMode : 1;    // [53]
			UINT32 DefaultBit : 1;  // [54]
			UINT32 Granularity : 1; // [55]
			UINT32 BaseHigh : 8;    // [56:63]
		} Fields;
	};
} SEGMENT_DESCRIPTOR, * PSEGMENT_DESCRIPTOR;
static_assert(sizeof(SEGMENT_DESCRIPTOR) == 8,
	"SEGMENT_DESCRIPTOR Size Mismatch");
#pragma warning(default:4201)

#pragma warning(disable:4201)
typedef struct _SEGMENT_ATTRIBUTE
{
	union
	{
		UINT16 AsUInt16;
		struct
		{
			UINT16 Type : 4;        // [0:3]
			UINT16 System : 1;      // [4]
			UINT16 Dpl : 2;         // [5:6]
			UINT16 Present : 1;     // [7]
			UINT16 Avl : 1;         // [8]
			UINT16 LongMode : 1;    // [9]
			UINT16 DefaultBit : 1;  // [10]
			UINT16 Granularity : 1; // [11]
			UINT16 Reserved1 : 4;   // [12:15]
		} Fields;
	};
} SEGMENT_ATTRIBUTE, * PSEGMENT_ATTRIBUTE;
static_assert(sizeof(SEGMENT_ATTRIBUTE) == 2,
	"SEGMENT_ATTRIBUTE Size Mismatch");
#pragma warning(default:4201)



#pragma warning(disable:4244)
#pragma warning(disable:4242)
#pragma warning(disable:4047)

UINT16
SvGetSegmentAccessRight(
	_In_ UINT16 SegmentSelector,
	_In_ ULONG_PTR GdtBase
)
{
	PSEGMENT_DESCRIPTOR descriptor;
	SEGMENT_ATTRIBUTE attribute;

	//
	// Get a segment descriptor corresponds to the specified segment selector.
	//
	descriptor = (
		GdtBase + (SegmentSelector & ~RPL_MASK));

	//
	// Extract all attribute fields in the segment descriptor to a structure
	// that describes only attributes (as opposed to the segment descriptor
	// consists of multiple other fields).
	//

	attribute.Fields.Type = descriptor->Fields.Type;
	attribute.Fields.System = descriptor->Fields.System;
	attribute.Fields.Dpl = descriptor->Fields.Dpl;
	attribute.Fields.Present = descriptor->Fields.Present;
	attribute.Fields.Avl = descriptor->Fields.Avl;
	attribute.Fields.LongMode = descriptor->Fields.LongMode;
	attribute.Fields.DefaultBit = descriptor->Fields.DefaultBit;
	attribute.Fields.Granularity = descriptor->Fields.Granularity;
	attribute.Fields.Reserved1 = 0;

	return attribute.AsUInt16;

}
#pragma warning(default:4047)
#pragma warning(default:4244)
#pragma warning(default:4242)

bool is_hv_setup = FALSE;

void verify_vmrun(VMCB* vmcb) {


	__int64 efer = __readmsr(CR_EFER);
	__int64 cr0 = __readcr0();
	//__int64 cr2 = __readcr2();
	__int64 cr4 = __readcr4();
	//__int64 cr3 = __readcr3();

	__int64 dr6 = __readdr(6);
	__int64 dr7 = __readdr(7);

	NT_ASSERT((efer >> 12) & 1);

	NT_ASSERT(!
		(
			((cr0 >> 30) & 1) &&
			(((cr0 >> 29) & 1) == 0)
		)
	);

	NT_ASSERT(cr0 >> 32 == 0);

	// ! Any MBZ bit of CR3 is set.
	// ! Any MBZ bit of CR4 is set.

	NT_ASSERT(dr6 >> 32 == 0);
	NT_ASSERT(dr7 >> 32 == 0);

	// ! EFER.LMA or EFER.LME is non-zero and this processor does not support long mode.

#pragma warning(disable:4554)
	NT_ASSERT(!
		(
		(efer >> 8) & 1 &&
		(efer >> 31) & 1 &&
		(cr4 >> 5) & 1 == 0
		)
	);

	NT_ASSERT(!
		(
			(efer >> 8) & 1 &&
			(efer >> 31) & 1 &&
			(cr4 & (1) == 0)
		)
	);
#pragma warning(default:4554)

	// ! EFER.LME, CR0.PG, CR4.PAE, CS.L, and CS.D are all non-zero

	NT_ASSERT(vmcb->ControlArea.InterceptMisc2 & SVM_INTERCEPT_MISC2_VMRUN);

	//! The MSR or IOIO intercept tables extend to a physical address that is greater than or equal to the maximum supported physical address.

	// ! Illegal event injection (section 15.20).

	NT_ASSERT(vmcb->ControlArea.GuestAsid != 0);

	// ! Any reserved bit is set in S_CET

	NT_ASSERT(!
		(cr4 >> 23 & 1) && (cr0 >> 16 & 1)
	);

	// ! CR4.CET=1 and U_CET.SS=1 when EFLAGS.VM=1

	// ! any reserved bit set in U_CET (SEV_ES only):

	//! VMRUN can load a guest value of CR0 with PE = 0 but PG = 1, a combination that is otherwise illegal

}

NTSTATUS
DriverEntry(
	PDRIVER_OBJECT DriverObject,
	PUNICODE_STRING RegistryPath
)
{
	UNREFERENCED_PARAMETER(RegistryPath);

	VCPU_INFO* vcpu;
	PCONTEXT guestContext;
	CR_EFER_t efer;
	DESCRIPTOR_TABLE_REGISTER gdtr, idtr;

	DriverObject->DriverUnload = DriverUnload;
	
	//DbgBreakPoint();
	
	log_debug_ok("Hello from HH!");
	
	log_debug_er("assuming host has SVM compat!\n");

	guestContext = ExAllocatePoolWithTag(NonPagedPool, sizeof(CONTEXT), HH_TAG);
	NT_ASSERT(guestContext != NULL);

	//capture guest context for vmrun later
	//this will be stored in gueststatearea when entering for the first time
	RtlCaptureContext(guestContext);
	

	log_debug_ok("checking if is_hv_setup\n");

	if (is_hv_setup == FALSE) {

		log_debug_ok("setting up hv now\n");

		//enable efer
		efer.value = __readmsr(CR_EFER);
		efer.fields.SVME = 1;

		__writemsr(CR_EFER, efer.value);

		
		//setup guest and host vmcb for processor
		vcpu = ExAllocatePoolWithTag(NonPagedPool, sizeof(VCPU_INFO), HH_TAG);
		NT_ASSERT(vcpu != NULL);


		vcpu->pa_guestvmcb =			MmGetPhysicalAddress(&vcpu->guest_vmcb);
		vcpu->pa_hostvmcb =				MmGetPhysicalAddress(&vcpu->host_vmcb);
		vcpu->pa_hoststatesavearea =	MmGetPhysicalAddress(&vcpu->HostStateSaveArea);

		NT_ASSERT((PVOID)vcpu->pa_guestvmcb.QuadPart == PAGE_ALIGN(vcpu->pa_guestvmcb.QuadPart));
		NT_ASSERT((PVOID)vcpu->pa_hostvmcb.QuadPart == PAGE_ALIGN(vcpu->pa_hostvmcb.QuadPart));
		NT_ASSERT((PVOID)vcpu->pa_hoststatesavearea.QuadPart == PAGE_ALIGN(vcpu->pa_hoststatesavearea.QuadPart));

		//this is where host state is saved on vmrun
		//restored on vmexit
		__try{
			__writemsr(SVM_MSR_VM_HSAVE_PA, vcpu->pa_hoststatesavearea.QuadPart);
		}
		__except (EXCEPTION_GP_FAULT) {
			log_debug_er("SVM_MSR_VM_HSAVE_PA not aligned. #GP\n");
			return STATUS_ABANDONED;
		}
		log_debug_ok("wrote hostvmcb.statesavearea to SVM_MSR_VM_HSAVE_PA\n");


		_sgdt(&gdtr);
		__sidt(&idtr);


		vcpu->guest_vmcb.ControlArea.GuestAsid = 1;
		vcpu->guest_vmcb.ControlArea.InterceptMisc1 |= SVM_INTERCEPT_MISC1_CPUID;
		vcpu->guest_vmcb.ControlArea.InterceptMisc2 |= SVM_INTERCEPT_MISC2_VMRUN;

		//segments
#pragma warning(disable:4244)
#pragma warning(disable:4242)
		vcpu->guest_vmcb.StateSaveArea.CsLimit = GetSegmentLimit(guestContext->SegCs);
		vcpu->guest_vmcb.StateSaveArea.DsLimit = GetSegmentLimit(guestContext->SegDs);
		vcpu->guest_vmcb.StateSaveArea.EsLimit = GetSegmentLimit(guestContext->SegEs);
		vcpu->guest_vmcb.StateSaveArea.SsLimit = GetSegmentLimit(guestContext->SegSs);
		vcpu->guest_vmcb.StateSaveArea.CsSelector = guestContext->SegCs;
		vcpu->guest_vmcb.StateSaveArea.DsSelector = guestContext->SegDs;
		vcpu->guest_vmcb.StateSaveArea.EsSelector = guestContext->SegEs;
		vcpu->guest_vmcb.StateSaveArea.SsSelector = guestContext->SegSs;
		vcpu->guest_vmcb.StateSaveArea.CsAttrib = SvGetSegmentAccessRight(guestContext->SegCs, gdtr.Base);
		vcpu->guest_vmcb.StateSaveArea.DsAttrib = SvGetSegmentAccessRight(guestContext->SegDs, gdtr.Base);
		vcpu->guest_vmcb.StateSaveArea.EsAttrib = SvGetSegmentAccessRight(guestContext->SegEs, gdtr.Base);
		vcpu->guest_vmcb.StateSaveArea.SsAttrib = SvGetSegmentAccessRight(guestContext->SegSs, gdtr.Base);
#pragma warning(default:4244)
#pragma warning(default:4242)

		//descriptors
		vcpu->guest_vmcb.StateSaveArea.GdtrBase = gdtr.Base;
		vcpu->guest_vmcb.StateSaveArea.GdtrLimit = gdtr.Limit;
		vcpu->guest_vmcb.StateSaveArea.IdtrBase = idtr.Base;
		vcpu->guest_vmcb.StateSaveArea.IdtrLimit = idtr.Limit;

		//cr
		vcpu->guest_vmcb.StateSaveArea.Cr0 = __readcr0();
		vcpu->guest_vmcb.StateSaveArea.Cr2 = __readcr2();
		vcpu->guest_vmcb.StateSaveArea.Cr3 = __readcr3();
		vcpu->guest_vmcb.StateSaveArea.Cr4 = __readcr4();
		vcpu->guest_vmcb.StateSaveArea.Efer = efer.value;

		//we only save rax because vmcb is passed through it
		vcpu->guest_vmcb.StateSaveArea.Rflags = guestContext->EFlags;
		vcpu->guest_vmcb.StateSaveArea.Rax = guestContext->Rax;
		vcpu->guest_vmcb.StateSaveArea.Rip = guestContext->Rip;

		//used for nested paging
		//vcpu->guest_vmcb.StateSaveArea.GPat =
		
		
		// Save some of the current state on VMCB. Some of those states are:
		// - FS, GS, TR, LDTR (including all hidden state)
		// - KernelGsBase
		// - STAR, LSTAR, CSTAR, SFMASK
		// - SYSENTER_CS, SYSENTER_ESP, SYSENTER_EIP
		// See "VMSAVE and VMLOAD Instructions" for mode details.
		//
		// Those are restored to the processor right before #VMEXIT with the VMLOAD
		// instruction so that the guest can start its execution with saved state,
		// and also, re-saved to the VMCS with right after #VMEXIT with the VMSAVE
		// instruction so that the host (hypervisor) do not destroy guest's state.
		//
		__svm_vmsave(vcpu->pa_guestvmcb.QuadPart);

		__svm_vmsave(vcpu->pa_hostvmcb.QuadPart);

		

		//this will make the guest skip the initialization branch
		is_hv_setup = TRUE;

		log_debug_ok("verifying vmrun constraints\n");
		verify_vmrun(&vcpu->guest_vmcb);

		//setup a new stack for host
		
		//vmrun loop!
		log_debug_ok("entering vmrun loop!\n");
		while (1) {
			//enter the vm
			__svm_vmrun(vcpu->pa_guestvmcb.QuadPart);
			//vmexit occurs here

			//save guest state
			__svm_vmsave(vcpu->pa_guestvmcb.QuadPart);
			RtlCaptureContext(guestContext);
			//reload host state
			__svm_vmload(vcpu->pa_hostvmcb.QuadPart);


			log_debug_ok("exit reason: %lld\n", vcpu->guest_vmcb.ControlArea.ExitCode);
			vcpu->guest_vmcb.StateSaveArea.Rip = vcpu->guest_vmcb.ControlArea.NRip;

			//save host state
			__svm_vmsave(vcpu->pa_guestvmcb.QuadPart);
			//restore guest state
			__svm_vmload(vcpu->pa_guestvmcb.QuadPart);
		}
		KeBugCheck(MANUALLY_INITIATED_CRASH);

	}


	return STATUS_SUCCESS;
}

